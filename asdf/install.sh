#!/bin/sh
if which brew >/dev/null 2>&1; then
    brew install asdf --HEAD
else
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.8
fi