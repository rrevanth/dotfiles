#!/bin/sh
if which brew >/dev/null 2>&1; then
	brew update && brew upgrade && brew cleanup
else
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi