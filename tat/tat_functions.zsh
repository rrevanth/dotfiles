tat() {
    if [[ -z $BOOKMARKS_FILE ]] ; then
    export BOOKMARKS_FILE="$HOME/.bookmarks"
    fi
    if [ $# -eq 0 ]; then
        local session_name=${PWD##*/} 
    else
        local session_name="$1"
    fi
    local sessions=( $(tmux list-sessions 2>/dev/null | cut -d ":" -f 1 | grep "^$session_name$") )

    if [ ${#sessions[@]} -gt 0 ]; then
        # If there is already a session with the same name, attach to it.
        tmux attach-session -t "$session_name"
    else
        # If there is no existing session, create a new (detached) one.
        tmux new-session -d -s "$session_name"

        # Try to find a matching code directory.
        local code_root_dirs=$(echo $CODE_ROOT_DIRS | sed 's/:/ /g')
        local search_dirs="$code_root_dirs"
        while IFS='' read -r line || [[ -n "$line" ]]; do
            search_dirs+=" ${line%%|*}"
        done < $BOOKMARKS_FILE
        if [ $# -ne 0 ]; then
            local jumpline=$(eval fd $session_name $search_dirs -H -t d -d 4 | $(fzfcmd) --bind=ctrl-y:accept --tac)
        fi
        echo "$session_name"
        echo "$jumpline"
        tmux send-keys -t "$session_name" "eval cd ${jumpline:=\"$PWD\" } && clear" C-m

        # If there is a .tmux file in this directory, execute it.
        if [ -f "$jumpline/.tmux" ]; then
            eval "$jumpline/.tmux" $session_name
        fi

        # Finally, attach to the newly created session.
        tmux attach-session -t "$session_name"
    fi
}

_tat() {
    COMPREPLY=()
    local session="${COMP_WORDS[COMP_CWORD]}"

    # For autocomplete, use both existing sessions as well as directory names.
    local sessions=( $(compgen -W "$(tmux list-sessions 2>/dev/null | awk -F: '{ print $1 }')" -- "$session") )

    COMPREPLY=( ${sessions[@]} )
}

# Added in .zshrc.after for fixing compdef errors
#complete -o filenames -o nospace -F _tat tat

