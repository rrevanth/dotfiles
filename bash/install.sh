#!/bin/sh
brew install fzf
brew install fd
brew install diff-so-fancy
brew install git-delta
brew install colordiff
brew install coreutils
brew install asdf --HEAD
brew install nvim
brew install zsh-completions
brew cask install android-platform-tools
brew install homebrew/cask-fonts/font-sourcecodepro-nerd-font-mono
brew install homebrew/cask-fonts/font-sourcecodepro-nerd-font
brew install gnupg
brew install gnu-sed
brew install dnsmasq