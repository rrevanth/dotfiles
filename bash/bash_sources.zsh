# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm


if [[ "$OSTYPE" == "darwin"* ]]; then
	# Android SDK export
	# export ANDROID_HOME=$HOME/Library/Android/sdk
	# export NDK_HOME=$ANDROID_HOME/ndk-bundle
	# export PATH=${PATH}:${ANDROID_HOME}/tools
	# export PATH=${PATH}:${ANDROID_HOME}/platform-tools

	# Add environment variable COCOS_CONSOLE_ROOT for cocos2d-x
	# export COCOS_CONSOLE_ROOT=$HOME/bin/cocos2d-x/tools/cocos2d-console/bin
	# export PATH=$COCOS_CONSOLE_ROOT:$PATH

	# Add environment variable COCOS_X_ROOT for cocos2d-x
	# export COCOS_X_ROOT=$HOME/bin
	# export PATH=$COCOS_X_ROOT:$PATH

	# Add environment variable COCOS_TEMPLATES_ROOT for cocos2d-x
	# export COCOS_TEMPLATES_ROOT=$HOME/bin/cocos2d-x/templates
	# export PATH=$COCOS_TEMPLATES_ROOT:$PATH

	# Add environment variable NDK_ROOT for cocos2d-x
	# export NDK_ROOT=$ANDROID_HOME/ndk-bundle
	# export PATH=$NDK_ROOT:$PATH

	# Add environment variable ANDROID_SDK_ROOT for cocos2d-x
	# export ANDROID_SDK_ROOT=$ANDROID_HOME
	# export PATH=$ANDROID_SDK_ROOT:$PATH
	# export PATH=$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools:$PATH

	# Add environment variable ANT_ROOT for cocos2d-x
	# export ANT_ROOT=/usr/local/Cellar/ant/1.10.1/bin
	# export PATH=$ANT_ROOT:$PATH
	
else
	#Virtual Env Variables
	export WORKON_HOME=$HOME/.envs
	export PROJECT_HOME=$HOME/workspace/envDevel
	#. /usr/share/virtualenvwrapper/virtualenvwrapper.sh

	#Android SDK
	# export ANDROID_HOME=$HOME/opt/androidSDK
	# export NDK_HOME=$HOME/opt/ndk-r10e
	# export PATH=$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$NDK_HOME:${PATH}
fi
# Make zsh always use default nvm alias in shell.To change node version permanently.change default with 'nvm alias default nodeversion'
# nvm use default > /dev/null

# ADD ~/opt to PATH
# for d in $HOME/opt/*/bin; do PATH="$d:$PATH"; done
# for d in $HOME/bin/*; do PATH="$d:$PATH"; done

# RVM specific installations
# [[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"
# export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting

# Adding PATHS
#for dir in $HOME/opt/*/bin; do [ -d $dir ] && export "PATH=$dir:$PATH"; done
# for d in $HOME/bin/*; do export PATH="$d:$PATH"; done
[ -d $HOME/bin ] && export PATH=$HOME/bin:$PATH
