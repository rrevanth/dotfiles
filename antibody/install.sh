#!/bin/sh
if which brew >/dev/null 2>&1; then
	brew install getantibody/tap/antibody || brew upgrade antibody
else
	curl -sL https://git.io/antibody | sh -s
fi

: > ~/.zsh_plugins.sh
for file in $DOTFILES/antibody/*.txt
do
  	antibody bundle < "$file" >> ~/.zsh_plugins.sh
done
antibody update