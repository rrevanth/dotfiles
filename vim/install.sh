#!/bin/sh
curl -sLf https://spacevim.org/install.sh | bash
vimdir="$HOME/.SpaceVim.d"
mkdir -p $vimdir; ln -sf $DOTFILES/vim/init.toml $vimdir 